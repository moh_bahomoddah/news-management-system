import * as firebase from 'firebase'
import 'firebase/firestore'
import router from '@/router'

export default{
  state: {
    loadedNewsItems: [ ]
  },
  mutations: {
    setLoadedNewsItems (state, payload) {
      state.loadedNewsItems = payload
    },
    createNewsItem (state, payload) {
      state.loadedNewsItems.push(payload)
    },
    updateNewsItem (state, payload) {
      const newsItem = state.loadedNewsItems.find(
        newsItem => 
       { return newsItem.id === payload.id
       })

      if (payload.title) {
        newsItem.title = payload.title 
      }
      if (payload.category) {
        newsItem.category = payload.category
      }
      if (payload.auther) {
        newsItem.auther = payload.auther
      }
      if (payload.description) {
        newsItem.description = payload.description
      }
      if (payload.date) {
        newsItem.date = payload.date
      } 
      if (payload.imageUrl) {
        newsItem.imageUrl = payload.imageUrl
      }
    },
    deleteNewsItem (state, payload) {
      const newsItem = state.loadedNewsItems.find(
        newsItem => 
       { return newsItem.id === payload.id
       })
      let i = state.loadedNewsItems.indexOf(newsItem)
      state.loadedNewsItems.splice(i,1)
    }
  },
  actions: {
    loadNewsItems ({commit}){
      commit('setLoading', true)
      const newsItems = []
      firebase.firestore().collection('newsItems').orderBy('date', 'desc').get()
      .then((qury) => {
        qury.docs.forEach((doc) => {
          if( doc.exists) {
            newsItems.push({
                id: doc.id,
                title: doc.data().title,
                auther: doc.data().auther,
                description: doc.data().description,
                category: doc.data().category,
                date: doc.data().date,
                imageUrl: doc.data().imageUrl
            })
            commit('setLoadedNewsItems', newsItems)
          } else {
            console.log('No such document!') 
          }
        })
        commit('setLoading', false)
      })
      .catch(error => {
        console.log(error)
        commit('setLoading', false)  
      })
    },
    createNewsItem ({commit }, payload) {  
      commit('setLoading', true)
      const newsItem = {
        title: payload.title,
        category: payload.category,
        imageUrl: payload.imageUrl,
        description: payload.description,
        date: payload.date,
        auther: payload.auther
      }
 
      let key
      firebase.firestore().collection('newsItems').add(newsItem)
      .then((doc) => {
        key = doc.id
      })
      .then(() => {
        commit('setLoading', false)
        commit('createNewsItem', { 
          ...newsItem,
          id: key })
        router.push('/newspage')
      })
      .catch((error) => {
        commit('setLoading', false)
        alert("Error adding document: ", error)
      })
    },
    updateNewsItem ({commit}, payload) {
      commit('setLoading', true)
      const updateobj = {}
      updateobj.id = payload.id
      if (payload.title) {
        updateobj.title = payload.title
      }
      if (payload.category) {
        updateobj.category = payload.category
      }
      if (payload.auther) {
        updateobj.auther = payload.auther
      }
      if (payload.description) {
        updateobj.description = payload.description
      }
      if (payload.date) {
        updateobj.date = payload.date
      }
      if (payload.imageUrl) {
        updateobj.imageUrl = payload.imageUrl
      }
      firebase.firestore().collection('newsItems').doc(payload.id).update(
        {  
            ...updateobj
        })
      .then(() => {
        commit('setLoading', false)
        commit('updateNewsItem', updateobj)
        router.push('/newspage/' + updateobj.id)
      })
      .catch(error => {
        console.log(error)
        commit('setLoading', false)
      })
    },
    deleteNewsItem ({commit}, payload) {
      commit('setLoading', true)
      firebase.firestore().collection('newsItems').doc(payload.id).delete()
      .then(() => {
        commit('setLoading', false)
        commit('deleteNewsItem',payload)
        router.push('/newspage')
      })
      .catch((error) => {
        console.log(error)
        commit('setLoading', false)
      })
    }

  }
}
