import * as firebase from 'firebase/app'
import 'firebase/firestore'
export default {
  state: {
    user: null
  },
  mutations: {
    setUser (state, payload) {
      state.user = payload
    }
  },
  actions: {
    signUserUp ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
      .then(
        user => {
          commit('setLoading', false)
          const newUser = {
            id: user.user.uid,
            name: payload.name,
            email: payload.email,
            type: 'user',
          }
          commit('setUser', newUser)
          firebase.firestore().collection('users').doc(newUser.id).set( {
            name: payload.name,
            email: payload.email,
            type: 'user',
            id: newUser.id
          })
        }
      )
      .catch(
        error => {
          commit('setLoading', false)
          commit('setError', error)
          console.log(error)
        }
      )
    },
    signUserIn ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
      .then(
        user => {
          commit('setLoading', false)
          firebase.firestore().collection('users')
          .doc(user.user.uid).get()
          .then(doc => {
            const newUser = {
            id: user.user.uid,
            name: doc.data().name,
            email: doc.data().email,
            type: doc.data().type,
          }
          commit('setUser', newUser) 
          }) 
        }
      )
      .catch(
        error => {
          commit('setLoading', false)
          commit('setError', error)
          console.log(error)
        }
      )
    },
    autoSignIn ({commit}, payload) {
      commit('setUser', payload)
    },
    logout ({commit}) {
      firebase.auth().signOut()
      commit('setUser', null)
    }
  }
}
